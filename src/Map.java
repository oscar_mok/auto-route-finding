import java.awt.Color;
import java.awt.Graphics;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Stack;

import javax.swing.JPanel;

public class Map extends JPanel
{
    static int [][] maze = new int[10][10]; //儲存迷宮(0或1或2)
    ArrayList arrayList = new ArrayList(); //儲存txt檔內容
    static Stack<NODE> stack = new Stack<NODE>(); //儲存路徑

    public class NODE  //inner class
    {
        int x;
        int y;
    }

    public Map()
    {
        readMap();
    }


    public void readMap()
    {

        //用scanner讀txt檔
        String path = "src/map.txt";
        Scanner scanner = null;
        try
        {
            scanner = new Scanner(Paths.get(path));

            //利用scanner把txt檔讀入arraylist
            while (scanner.hasNext())
            {
                arrayList.add(scanner.nextLine());
            }

            //先設定原先每個皆為0
            for(int k=0; k<10; k++)
            {
                for(int l=0; l<10; l++)
                {
                    maze[k][l]=0;
                }
            }

            //讀txt檔內容，並把相對應的陣列設成1(牆壁):txt檔中row和col的值都要減1，才存入陣列中
            for(int i = 0; i< arrayList.size(); i++)
            {
                String input = (String) arrayList.get(i);
                String[] content = input.split(" ");

                for(int j=0; j<content.length; j++)
                {
                    int num_j = Integer.parseInt(content[j])-1;
                    maze[i][num_j]=1;
                }
            }

            //設定起點和終點是2
            maze[0][0]=2;
            maze[Q3_105403031.getX_finish_point()][Q3_105403031.getY_finish_point()]=2;
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        scanner.close();
    }

    public void backtracking()
    {
        boolean check = false;
        int row=0;
        int col=0;

        // 將起始點設為3 以免走回頭路
        //maze[row][col]=maze[y][x]

        maze[0][0] = 3;

        do
        {
            if(col+1<10&&(maze[row][col+1]==0 || maze[row][col+1]== 2)) //向右走
            {
                maze[row][col+1]=3; //設定此格已走過
                check=true;
                col=col+1;
            }
            else if(col-1>=0&&(maze[row][col-1]==0 || maze[row][col-1]==2)) //向左走
            {
                maze[row][col-1]=3; //設定此格已走過
                check=true;
                col=col-1;
            }
            else if(row+1<10&&(maze[row+1][col]==0 || maze[row+1][col]==2)) //向下走
            {
                maze[row+1][col]=3;
                check=true;
                row=row+1;
            }
            else if(row-1>=0&&(maze[row-1][col]==0 || maze[row-1][col]==2)) //向上走
            {
                maze[row-1][col]=3;
                check=true;
                row=row-1;
            }
            else
            {
                check=false;//上下左右都無法行走的時候 check 為 false
            }

            if(check==true)
            {
                //有路可以走
                NODE node= new NODE();
                node.x = col;
                node.y = row;
                stack.push(node);
            }
            else
            {
                //沒路可以走
                stack.pop();
                if(!stack.isEmpty())
                {
                    col = stack.peek().x;
                    row = stack.peek().y;
                    maze[row][col]=3;
                }
            }

            if(row == Q3_105403031.getX_finish_point() && col == Q3_105403031.getY_finish_point())
            {
                //到了邊界，跳出迴圈
                break;
            }
        }while(true);

        stack.pop();
    }

    public void paint(Graphics g)
    {
        maze[0][0]=2;
        maze[Q3_105403031.getX_finish_point()][Q3_105403031.getY_finish_point()]=2;

        super.paint(g);
        g.translate(10, 40);

        for(int row=0; row<10; row++)
        {
            for(int col=0; col<10; col++)
            {
                Color color;
                switch(maze[row][col])
                {
                    case 1: color=Color.BLACK; break;
                    case 2: color=Color.BLUE; break;
                    default: color=Color.WHITE;
                }
                g.setColor(color);
                g.fillRect(50*col, 50*row, 50, 50);
                g.setColor(Color.BLACK);
                g.drawRect(50*col, 50*row, 50, 50);
            }
        }


        while(true)
        {
            if(!stack.isEmpty())
            {
                NODE node = stack.pop();
                int pathY = node.y;
                int pathX = node.x;
                g.setColor(Color.RED);
                g.fillRect(pathX*50, pathY*50, 50, 50);
            }
            else
            {
                break;
            }
        }
    }
}

