import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class Q3_105403031 extends JFrame
{
    private static JTextArea x_finish_point;
    private static JTextArea y_finish_point;
    private static int x_finish_point_int = 0;
    private static int y_finish_point_int = 0;
    private JButton run;
    private JPanel north_holder = new JPanel();
    Map mazemap = new Map();

    public Q3_105403031()
    {

        north_holder.setLayout(new FlowLayout(FlowLayout.CENTER));
        north_holder.add(new JLabel("選擇終點(x,y)："));
        x_finish_point = new JTextArea();
        x_finish_point.setPreferredSize(new Dimension(20, 20));
        y_finish_point = new JTextArea();
        y_finish_point.setPreferredSize(new Dimension(20, 20));

        north_holder.add(x_finish_point);
        north_holder.add(y_finish_point);



        //設定run button
        run = new JButton("run");
        ButtonHandler handler = new ButtonHandler();
        run.addActionListener(handler);

        add(run, BorderLayout.SOUTH);
        add(north_holder, BorderLayout.NORTH);
        add(mazemap, BorderLayout.CENTER);
    }

    //當按下run按鈕，則執行mazemap class中的backtracking方法，並repaint
    private class ButtonHandler implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent event)
        {
            x_finish_point_int = Integer.parseInt(x_finish_point.getText());
            y_finish_point_int = Integer.parseInt(y_finish_point.getText());

            mazemap.backtracking();
            mazemap.repaint();

            run.setEnabled(false);
        }
    }

    public static int getX_finish_point() {
        return x_finish_point_int;
    }

    public static int getY_finish_point() {
        return y_finish_point_int;
    }
}

